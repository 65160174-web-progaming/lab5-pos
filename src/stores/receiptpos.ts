import { ref, computed } from 'vue'
import { defineStore } from 'pinia'


import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import { useMemberStore } from './member';

export const useReceiptPOSStore = defineStore('receiptPOS', () => {
  const authStore = useAuthStore();
  const memberStore = useMemberStore();
  const receiptDialog = ref(false);
  const receipt = ref<Receipt>({
    id: 0,
    createdDate: new Date(),
    total: 0,
    memberDiscount: 0,
    netTotal: 0,
    receiptAmount: 0,
    change: 0,
    paymentTyp: 'cash',
    userId: authStore.currentUser.id,
    user: authStore.currentUser,
    memberId: 0,
    totalItem: 0,
  })
  const receiptItems = ref<ReceiptItem[]>([])
  const addReceiptItem = (product: Product) => {
    const index = receiptItems.value.findIndex((item) => item.product?.id === product.id)
    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
      return
    } else {
      const newReceipt: ReceiptItem = {
        id: -1,
        name: product.name,
        price: product.price,
        unit: 1,
        productId: product.id,
        product: product
      }
      receiptItems.value.push(newReceipt)
      calReceipt()
    }
  }
  const removeReceiptItem = (receiptItem: ReceiptItem) => {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }
  const increaseItem = (item: ReceiptItem) => {
    item.unit++
    calReceipt()
  }
  function decreaseItem(item: ReceiptItem) {
    if (item.unit === 1) {
      removeReceiptItem(item)
    }
    item.unit--
    calReceipt()
  }
 
  function calReceipt() {
    let total = 0
    let totalItem = 0
    for (const item of receiptItems.value) {
      total = total + (item.price * item.unit)
      totalItem = totalItem + item.unit
    }
    receipt.value.totalItem = totalItem
    receipt.value.total = total
    if (memberStore.currentMember) {
      receipt.value.netTotal = total * 0.95
    } else {
      receipt.value.netTotal = total
    }

  }

  function showDialog() {
    receipt.value.receiptItems = receiptItems.value
    receiptDialog.value = true
    
  }

  function clear(){
    receiptItems.value = []
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      total: 0,
      memberDiscount: 0,
      netTotal: 0,
      receiptAmount: 0,
      change: 0,
      paymentTyp: 'cash',
      userId: authStore.currentUser.id,
      user: authStore.currentUser,
      memberId: 0,
      totalItem: 0
    }
    memberStore.clear()
  }

  return {
    receiptItems, receipt,receiptDialog,
    addReceiptItem, removeReceiptItem, increaseItem, decreaseItem,calReceipt, showDialog, clear,
  }
})
