import type { Member } from "./Member";
import type { Product } from "./Product";
import type { ReceiptItem } from "./ReceiptItem";
import type { User } from "./User";

type Receipt ={
    id: number;
    createdDate: Date;
    total: number;
    totalItem: number;
    memberDiscount: number;
    netTotal: number;
    receiptAmount: number;
    change: number;
    paymentTyp: string;
    userId: number;
    user?: User;
    memberId: number;
    member?: Member;
    receiptItems?: ReceiptItem[];    
    
}

export type {Receipt}